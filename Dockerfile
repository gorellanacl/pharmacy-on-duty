FROM node:12.14.0-alpine

# Create app dir
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app

# Dependencies
COPY package*.json ./

USER node

# Production
# RUN npm ci --only=production
RUN npm install

# Bundle app source
COPY --chown=node:node . .

EXPOSE 8080
# Runtime
CMD [ "node", "index.js" ]