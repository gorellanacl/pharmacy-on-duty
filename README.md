# Pharmacies on Duty
---
Aplicación que consume API públicas de farmacias, para disponer de servicios que permiten conocer antecedentes de estos.

---
## Características
1. Solución totalmente "dockerizada"
2. Implementa Google Maps API para visualizar mapas
3. Desarrollado en Node.js
4. Interfaz simple para trabajar con la aplicación

## Requerimientos

- Máquina, ojalá, con distribución Linux como SO (Ubuntu 18.04 se utilizó para el desarrollo de la aplicación)
- Docker instalado en la máquina donde se vaya a probar la aplicación. __[Descargar e instalar Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)__

## Contenedor

- __[Docker Hub](https://hub.docker.com/repository/docker/goc00/pharmacy-on-duty)__ - Contenedor público donde se encuentra la aplicación completa.

## Puesta en marcha
Al ser una solución "dockerizada", lo único que habría que hacer es:

a. Descargar imagen (pública)
``` docker
$ docker pull goc00/pharmacy-on-duty
```

b. Con la imagen descargada, correr contenedor sobre puerto 8888 (en rigor, podría ser cualquiera)
``` docker
$ docker run --name pharmacy-on-duty -p 8888:8080 -d goc00/pharmacy-on-duty
```

c. Acceder a __http://localhost:8888__

d. __IMPORTANTE__: Si por algún motivo no levantara a través de http://localhost:8888, por favor realizar lo siguiente:

- Verificar la IP por donde Docker está haciendo el forwarding
``` docker
$ docker-machine ip
```

- Con la IP obtenida, acceder a: http://__[IP_OBTENIDA]__:8888