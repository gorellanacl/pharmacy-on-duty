const express = require('express');
const routes = require('./v1/routes/PharmacyRoute');

const router = express.Router();

/**
 * Routes related to entire public API
 * This central point process sub-routes
 */
router.use('/api/v1', routes);

module.exports = router;