const rp = require('request-promise');

class MinsalModel {

    /**
     * Get all counties by region, returning a HTML structure with all options
     * 
     * @param {*} regionId 
     * 
     * @returns HTML string
     */
    async getCounties (regionId) {
        try {
        
            const opts = {
                method: 'POST',
                uri: global.gConfig.endpoints.getCounties.url,
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                formData: {
                    reg_id: regionId.toString()
                }, json:false
            };
            const res = await rp(opts);
            console.log(res);
            if(!res) { throw { message: 'Could not get counties information from minsal service' } }

            // HTML response
            return res;

        } catch(e) { //
            console.error(e);
            return false;
        }
    }

    /**
     * Get all pharmacies on duty by region, returning a list of objects
     * 
     * @param {*} regionId 
     * 
     * @return List<Object>
     */
    async getPharmaciesOnDuty(regionId) {

        try {
        
            const opts = {
                method: 'GET',
                uri: `${global.gConfig.endpoints.getPharmaciesOnDuty.url}${regionId}`
            };
            const res = await rp(opts);
            console.log(res);
            if(!res) { throw { message: 'Could not get pharmacies information from minsal service' } }

            // OK
            return JSON.parse(res);

        } catch(e) { //
            console.error(e);
            return false;
        }

    }

}

module.exports = new MinsalModel();