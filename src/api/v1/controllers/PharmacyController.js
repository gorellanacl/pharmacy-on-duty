const minsal = require('./../services/MinsalService');
const cache = require('memory-cache');

class PharmacyController {

    /**
     * Initialize objects
     */
    async init(req, res) {

        // Will load objects and store them as cache, in order to load one time only

        try {

            const counties = !cache.get('counties') ? await minsal.getCounties() : cache.get('counties');
            const pharmacies = !cache.get('pharmacies') ? await minsal.getPharmaciesOnDuty() : cache.get('pharmacies');

            res.json({
                counties: counties,
                pharmacies: pharmacies
            });

        } catch(e) {
            console.error(e);
        }
    }

    /**
     * Get all 
     * 
     * @param {*} req 
     * @param {*} res 
     */
    async getPharmacies(req, res) {

        let success = true;
        let message = '';
        let data = [];

        try {

            // Receive params
            const body = req.body;
            if(!body) { throw { message: 'Body request is empty' } }

            const countyId = body.countyId.toString();
            const countyName = body.countyName.toString().trim().toUpperCase();
            const localName = body.localName.toString().trim().toUpperCase();

            if(countyId === '0') { throw { message: 'You need to choose a county at least' } }

            // Will find elements
            const objs = !cache.get('pharmacies') ? await minsal.getPharmaciesOnDuty() : cache.get('pharmacies');
            const filtered = [];

            // Searching type
            const withLocalName = localName !== '' ? true : false;

            objs.data.forEach(element => {

                // At this point county was selected, so we need to ask if local name was set or not 
                const eleCountyName =  element.comuna_nombre.toUpperCase();
                const eleName =  element.local_nombre.toUpperCase();

                if(withLocalName) {
                    if(eleName.includes(localName) && (countyName === eleCountyName)) {
                        filtered.push(element);
                    }
                } else {
                    if(countyName === eleCountyName) {
                        filtered.push(element);
                    }
                }

            });

            // OK, it return filtered pharmacies
            data = filtered;

        } catch(e) {
            console.error(e);
            success = false;
            message = e.message;
        }

        // Response
        res.json({
            success: success,
            message: message,
            pharmacies: data
        })

    }

}

module.exports = new PharmacyController();