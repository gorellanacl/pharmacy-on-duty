const service = require('./../services/MinsalService');

class MinsalController {

    /**
     * Get all counties by region id
     * By default and if id is not set, id will be ENDPOINTS_COUNTIES_DEFAULT_ID
     * 
     * @param {*} req 
     * @param {*} res 
     */
    async getCountiesByRegion (req, res) {

        let call = null;

        try {

            call = await service.getCounties(req.params.regionId || global.gConfig.endpoints.defaultRegionId);
            res.send(call);

        } catch(e) {
            console.log(e);
            res.status(500).send(call);
        }
 
    }


    /**
     * Get all pharmacies on duty by region
     * 
     * @param {*} req 
     * @param {*} res 
     */
    async getPharmaciesOnDutyByCounty (req, res) {
        
        let call = null;

        try {

            call = await service.getPharmaciesOnDuty(req.params.regionId || global.gConfig.endpoints.defaultRegionId);
            res.json(call);

        } catch(e) {
            console.log(e);
            res.status(500).json(call);
        }

    }

}

module.exports = new MinsalController();