const model = require('../models/MinsalModel');
const Response = require('./../dto/JsonResponse');

class MinsalService {

    async getCounties (regionId) {

        let result = new Response();

        try {

            const res = await model.getCounties(regionId || global.gConfig.endpoints.defaultRegionId);
            
            if(!res) { throw { message: 'No response from Minsal service' } }
            if(res === false) { throw { message: 'Could not get counties information' } }

            result._success = true;
            result._data = res;

        } catch(e) {
            console.error(e);
        }

        return result;
        
    }

    /**
     * Get all pharmacies by region from minsal service
     * 
     * @param {*} regionId 
     * 
     * @returns null | List<Object>
     */
    async getPharmaciesOnDuty (regionId) {

        let result = new Response();

        try {

            const res = await model.getPharmaciesOnDuty(regionId || global.gConfig.endpoints.defaultRegionId);
            
            if(!res) { throw { message: 'No response from Minsal service' } }
            if(res === false) { throw { message: 'Could not get pharmacies information' } }

            result._success = true;
            result._data = res;

        } catch(e) {
            console.error(e);
        }

        return result;
        
    }

}

module.exports = new MinsalService();