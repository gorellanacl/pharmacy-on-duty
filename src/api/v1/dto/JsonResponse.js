/**
 * Standard response to any service
 */

class JsonResponse {

    constructor() {
        this.success = false;
        this.message = 'No message';
        this.data = {};
    }

    set _success(success) { this.success = success; }
    set _message(message) { this.message = message; }
    set _data(data) { this.data = data; }

    get _success() { return this.success; }
    get _message() { return this.message; }
    get _data() { return this.data; }
}

module.exports = JsonResponse;