const express = require('express');
const controller = require('./../controllers/PharmacyController');

const router = express.Router();

/**
 * Public endpoints
 */
router.get('/init', controller.init);
router.post('/pharmacies', controller.getPharmacies);

module.exports = router;