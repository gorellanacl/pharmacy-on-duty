/**
 * Simple js scripts for displaying interface
 */

const API = '/api/v1';
let map;
let marker;

$(document).ready(function() {
    
    // Loading initial elements
    const counties = $('#countiesList');
    const pharmacies = $('#pharmaciesGrid');
    const searching = $("#searching");

    counties.prop('disabled', 'disabled');

    $.get(
        `${API}/init`,
        function(res) {
            console.log(res);

            // Counties list
            counties.empty()
                    .html(res.counties.data)
                    .prop('disabled', false);

            // Pharmacies
            loadTable(pharmacies, res.pharmacies.data);

            // Load first pharmacy in map
            if(res.pharmacies.data.length > 0) {
                const firstPharmacy = res.pharmacies.data[0];
                initMap(firstPharmacy.local_lat, firstPharmacy.local_lng, firstPharmacy.local_nombre);
            }

            searching.hide();
        },
        'json'
    );

    // Search
    $('#filterForm').submit(function(e) {
        
        const btn = $("#submitBtn");
   
        btn.prop("disabled", "disabled");
        searching.show();

        $.post(
            `${API}/pharmacies`,
            {
                'countyId': counties.val(),
                'countyName': $('#countiesList option:selected').text(),
                'localName': $('#localName').val()
            },
            function(res) {
                console.log(res);
                if(res.success) {
                    loadTable(pharmacies, res.pharmacies);
                } else {
                    alert(res.message);
                }

                btn.prop("disabled", false);
                searching.hide();
                
            }, 'json'
        );

        e.preventDefault();
    });

    // Show map
    $(document).on("click", ".showMap", function(e) {
        const lat = $(this).data("lat");
        const lng = $(this).data("lng");
        const name = $(this).data("name");
        
        initMap(lat, lng, name);

        e.preventDefault();
    });

});

// Load table with elements from received array
function loadTable(tbl, arr) {

    tbl.find("tbody tr").remove();

    $("#total").html(arr.length);

    arr.forEach(function(element) {
        tbl.children("tbody").append('<tr>'+
            '<td>'+element.local_nombre+'</td>'+
            '<td>'+element.local_direccion+'</td>'+
            '<td>'+element.local_telefono+'</td>'+
            '<td>'+element.local_lat+'</td>'+
            '<td>'+element.local_lng+'</td>'+
            '<td><a href="#" class="showMap" data-lat="'+element.local_lat+'" data-lng="'+element.local_lng+'" data-name="'+element.local_nombre+'">Ver</a></td>'+
        '</tr>');
    });
}

// Load marker into map
function initMap(latX, lngX, localName) {

    if(latX == '' || lngX == '') {
        alert("Cannot show map because coords weren't set");
        return;
    }

    const coords = {lat: parseFloat(latX), lng: parseFloat(lngX)};

    map = new google.maps.Map(document.getElementById('map'), {
        center: coords,
        zoom: 17
    });

    marker = new google.maps.Marker({
        position: coords,
        map: map,
        title: localName
    });
}