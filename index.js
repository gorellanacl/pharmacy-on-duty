'use strict';

/**
 * ----------------
 * Pharmacy on duty
 * ----------------
 * 
 * Main file with initial services for Consorcio technical test
 * API consumes public information from minsal
 * Dockerize application in order to implement a better solution
 * 
 * @author  Gastón Orellana C. <gaston.orellana@live.cl>
 * @since   1.0.0
 */

require('dotenv').config();

const config = require('./config/config');
const app = require('./config/server');

// Global configuration object. This way will load file one time
global.gConfig = config;

app.listen(config.app.port, () => {
    console.log(`Running app on port ${config.app.port}`);
});

