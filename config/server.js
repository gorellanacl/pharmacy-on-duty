/**
 * Serving application with express framework
 */
const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');

// API routes
const routes = require('./../src/api/routes');

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(__dirname + '/./../static'));
app.use('/', routes);
app.get('/ping', (req, res) => res.send('pong'));

module.exports = app;