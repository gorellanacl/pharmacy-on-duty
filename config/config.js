const app = {
    port: process.env.APP_PORT || 8080
};
const endpoints = {
    defaultRegionId: process.env.DEFAULT_REGION_ID || 7,
    getCounties: {
        url: process.env.ENDPOINTS_COUNTIES || 'https://midastest.minsal.cl/farmacias/maps/index.php/utilidades/maps_obtener_comunas_por_regiones'
    },
    getPharmaciesOnDuty: {
        url: process.env.ENDPOINTS_PHARMS_ON_DUTY || 'https://farmanet.minsal.cl/maps/index.php/ws/getLocalesRegion?id_region='
    }
}

module.exports = {
    app,
    endpoints
};
